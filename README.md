# Vue Weather App

### Check Your City Weather

#### Web Technologies I Used

![Vue js](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/18ee2fa0ef68e42cab7611df750f99d040ee62f9/img/Vue.svg)![Netlify](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/3b7e2c6db845f61b47fd4bcc334c53c2094ee6ef/img/netlify.svg)

### Feature

1. Written In Vue

2. Api Used [openweathermap](openweathermap.org)

---

### Future Feature

1. Adding 7 day Weather

2. Better UI/UX

3. Adding Loading Bar

4. Error Page

5. Map Of Your Locaton

6. Dark Mode

---

Click To see :- [Website](https://weather-vuejs-app.netlify.app)

Reference link :- [https://www.youtube.com/watch?v=JLc-hWsPTUY](https://www.youtube.com/watch?v=JLc-hWsPTUY)

---

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

---

### For Netlify Hosting

#### initializing

```
# auto initializing
netlify init

# manual initializing
netlify init --manual
```

#### Deploy

```
#Draft
netlify deploy

#Porduction
netlify deploy --prod
```

---

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
See [Netlify Configuration Reference](https://docs.netlify.com/cli/get-started/)
